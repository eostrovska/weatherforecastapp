# README #

Test project for Software Company based in Kiev, UA

### What is this project about? ###

ASP.NET MVC as an API with simple SPA to get weather info and weather forecast for 5 days using OpenWeatherAPI

### How do I get set up? ###

* Open solution file in VisualStudio 2015+
* Wait until NuGet package manager restore all needed packages
* Start the application