﻿(function () {
    //Get current weather data click event handler
    $("#getWeather").click(function () {
        $("#weatherForecast").addClass("hidden");

        var selectedCityId = $('#cityNameDd').find(":selected").val();
        $.get(getWeatherUrl + "?cityId=" + selectedCityId, function (response) {
            $(".cityName").text(response.Data.Name);
            $(".countryName").text(response.Data.Sys.Country);
            $("#mainTemp").text(response.Data.Main.Temp);
            $("#weatherMain").text(response.Data.Weather[0].Main);
            $("#weatherDescription").text(response.Data.Weather[0].Description);
            $("#weatherIcon").attr("src", "http://openweathermap.org/img/w/" + response.Data.Weather[0].Icon + ".png");

            $("#weatherToday").removeClass("hidden");
        })
            .fail(function (data) {
                console.log(data);
            });
    });

    //Get 5 days forecast weather data click event handler
    $("#getForecast").click(function () {
        $("#weatherToday").addClass("hidden");
        $("#weatherForecastRowContainer").empty();

        var selectedCityId = $('#cityNameDd').find(":selected").val();
        $.get(getForecastUrl + "?cityId=" + selectedCityId, function (response) {
            $("#weatherForecast").removeClass("hidden");

            $(".cityName").text(response.Data.City.Name);
            $(".countryName").text(response.Data.City.Country);

            $.each(response.Data.List, function (index, value) {
                
                var template = $("#hidden-template").html();

                template = template.replace("{DATE_TIME}", new Date(value.Dt*1000));
                template = template.replace("{ICON}", value.Weather[0].Icon);
                template = template.replace("{TEMPERATURE}", value.Main.Temp);
                template = template.replace("{WIND_SPEED}", value.Wind.Speed);
                template = template.replace("{PRESSURE}", value.Main.Pressure);

                $("#weatherForecastRowContainer").append(template);
            });
        })
            .fail(function (data) {
                console.log(data);
            });
    });
})();