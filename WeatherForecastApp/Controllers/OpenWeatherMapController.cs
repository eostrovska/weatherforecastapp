﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using WeatherForecastApp.Models;

namespace WeatherForecastApp.Controllers
{
    public class OpenWeatherMapController : Controller
    {
        private const string API_KEY = "fd0e89d7a36bfb826d77fbd9deab5935";

        // GET: OpenWeatherMapMvc
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Get data about current weather condition by city id using the HTTP request from OpenWeatherAPI.  
        /// </summary>
        /// <param name="cityId">OpenWeatheAPI city Id</param>
        /// <returns>Current weather data in json format.</returns>
        [HttpGet]
        [Route("weather/{cityId?}")]
        public JsonResult GetWeatherDataByCityId(string cityId)
        {
            var response = new ResponseViewModel();

            if (cityId != null)
            {
                HttpWebRequest apiRequest = WebRequest.Create("http://api.openweathermap.org/data/2.5/weather?id=" +
                                                                cityId + "&appid=" + API_KEY + "&units=metric") as HttpWebRequest;

                string apiResponse;
                using (HttpWebResponse httpResponse = apiRequest.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                    apiResponse = reader.ReadToEnd();
                }

                var weatherData = JsonConvert.DeserializeObject<ResponseWeatherModel>(apiResponse);

                response.Message = "Weather data for " + weatherData.Name + " received successfully!";
                response.Data = weatherData;
                response.Error = false;
            }
            else
            {
                response.Message = "No city id received";
                response.Error = true;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Get data about weather condition for 5 days by city id using the HTTP request from OpenWeatherAPI.
        /// </summary>
        /// <param name="cityId">OpenWeatheAPI city Id</param>
        /// <returns>5 days forecast weather data in json format.</returns>
        [HttpGet]
        [Route("weather2/{cityId?}")]
        public JsonResult GetForecastWeatherDataByCityId(string cityId)
        {
            var response = new ResponseViewModel();

            if (cityId != null)
            {
                HttpWebRequest apiRequest = WebRequest.Create("http://api.openweathermap.org/data/2.5/forecast?id=" +
                                                               cityId + "&appid=" + API_KEY + "&units=metric") as HttpWebRequest;

                string apiResponse;
                using (HttpWebResponse httpResponse = apiRequest.GetResponse() as HttpWebResponse)
                {
                    StreamReader reader = new StreamReader(httpResponse.GetResponseStream());
                    apiResponse = reader.ReadToEnd();
                }

                var weatherData = JsonConvert.DeserializeObject<FiveDayWeatherForecastModel>(apiResponse);

                response.Message = "Weather data for " + weatherData.City + " received successfully!";
                response.Data = weatherData;
                response.Error = false;
            }
            else
            {
                response.Message = "No city id received";
                response.Error = true;
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}