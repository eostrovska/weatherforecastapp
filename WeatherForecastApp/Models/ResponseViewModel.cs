﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecastApp.Models
{
    public class ResponseViewModel
    {
        public string Message { get; set; }
        public bool Error { get; set; }
        public object Data { get; set; }
    }
}