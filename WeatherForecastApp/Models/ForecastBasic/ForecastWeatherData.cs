﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherForecastApp.Models.OneDayWeatherBasic;

namespace WeatherForecastApp.Models.ForecastBasic
{
    public class ForecastWeatherData
    {
        public int Dt { get; set; }
        public Main Main { get; set; }
        public List<Weather> Weather { get; set; }
        public Clouds Clouds { get; set; }
        public Wind Wind { get; set; }
        public Sys Sys { get; set; }
        public string Dt_txt { get; set; }
    }
}