﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherForecastApp.Models.OneDayWeatherBasic;

namespace WeatherForecastApp.Models.ForecastBasic
{
    public class City
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Coordinates Coordinates { get; set; }
        public string Country { get; set; }
    }
}