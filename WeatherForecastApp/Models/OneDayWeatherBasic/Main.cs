﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecastApp.Models.OneDayWeatherBasic
{
    public class Main
    {
        public double Temp { get; set; }
        public int Pressure { get; set; }
        public int Humidity { get; set; }
        public int Temp_min { get; set; }
        public int Temp_max { get; set; }
    }
}