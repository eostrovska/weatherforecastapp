﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecastApp.Models.OneDayWeatherBasic
{
    public class Wind
    {
        public double Speed { get; set; }
        public double Deg { get; set; }
    }
}