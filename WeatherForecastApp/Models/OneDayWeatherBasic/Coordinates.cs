﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WeatherForecastApp.Models.OneDayWeatherBasic
{
    public class Coordinates
    {
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }
}