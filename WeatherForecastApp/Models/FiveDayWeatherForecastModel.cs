﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WeatherForecastApp.Models.ForecastBasic;

namespace WeatherForecastApp.Models
{
    public class FiveDayWeatherForecastModel
    {
        public int Cod { get; set; }
        public double Message { get; set; }
        public int Cnt { get; set; }
        public List<ForecastWeatherData> List { get; set; }
        public City City { get; set; }
    }
}